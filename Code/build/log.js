export function log(message = null, type = "log") {
	window.logs = window.logs || []
	if (message != null) {
		window.logs.push({
			"id": window.logs.length + 1,
			"message": message,
			"type": type 
		})
	} else {
		return window.logs
	}
}