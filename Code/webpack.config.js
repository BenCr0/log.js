const path = require('path');
const CleanWebpackPlugin = require("clean-webpack-plugin");
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

const build_folder = "./build/"
const production_folder = "production"

const config = {
	entry: {
		log: `${build_folder}log.js`
	},
	output: {
		filename: '[name].js',
		path: path.resolve(__dirname, production_folder)
	},
	mode: 'production',
	devtool: false,
	optimization: {
		splitChunks: {
			chunks: 'all'
		}
	},
	module: {
		rules: [
		{
			test: /\.js$/,
			exclude: /node_modules\/(?!icarus).*/,
			use: {
				loader: 'babel-loader'
			}
		}
		]
	},
	plugins: [
	new CleanWebpackPlugin([production_folder]),
	//new BundleAnalyzerPlugin()
	]
};

module.exports = config;